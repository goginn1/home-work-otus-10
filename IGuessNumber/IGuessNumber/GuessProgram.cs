﻿using System;

namespace IGuessNumber
{
    // Принцип единственной ответственности
    // Отдельный класс для генерации случайных чисел
    public class RandomNumberGenerator
    {
        private readonly Random _random = new Random();

        public int Generate(int min, int max)
        {
            return _random.Next(min, max + 1);
        }
    }

    // Принцип разделения интерфейса
    // Отдельный интерфейс для ввода данных
    public interface IUserInput
    {
        int ReadNumber();
    }

    // Реализация интерфейса для консольного ввода данных
    public class ConsoleUserInput : IUserInput
    {
        public int ReadNumber()
        {
            return int.Parse(Console.ReadLine());
        }
    }

    // Принцип инверсии зависимостей
    // Класс игры зависит от абстракции IUserInput, а не от конкретной реализации
    public class GuessNumberGame
    {
        private readonly int _min;
        private readonly int _max;
        private readonly int _targetNumber;
        private readonly IUserInput _userInput;

        public GuessNumberGame(int min, int max, IUserInput userInput)
        {
            _min = min;
            _max = max;
            _targetNumber = new RandomNumberGenerator().Generate(_min, _max);
            _userInput = userInput;
        }

        public void Start()
        {
            int attempts = 0;
            int guess;

            Console.WriteLine($"Угадайте число от {_min} до {_max}:");

            do
            {
                guess = _userInput.ReadNumber();
                attempts++;

                if (guess < _targetNumber)
                    Console.WriteLine("Больше");
                else if (guess > _targetNumber)
                    Console.WriteLine("Меньше");
                else
                    Console.WriteLine($"Поздравляю! Вы угадали число {_targetNumber} за {attempts} попыток.");

            } while (guess != _targetNumber);
        }
    }

    class GuessProgram
    {
        static void Main(string[] args)
        {
            // Принцип подстановки Барбары Лисков
            // Можно легко заменить реализацию IUserInput на другую, не нарушая работу приложения
            IUserInput userInput = new ConsoleUserInput();

            int min = 1; // Минимальное значение числа
            int max = 100; // Максимальное значение числа

            // Принцип открытости/закрытости
            // Параметры игры задаются из настроек
            GuessNumberGame game = new GuessNumberGame(min, max, userInput);
            game.Start();
        }
    }
}